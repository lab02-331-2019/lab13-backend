package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.springframework.security.core.parameters.P;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Lecturer extends Person {

    @OneToMany(mappedBy = "advisor")
    @Builder.Default
    @ToString.Exclude
    List<Student> advisees = new ArrayList<>();
    @OneToMany(mappedBy = "lecturer")
    @Builder.Default
    @JsonManagedReference
    @ToString.Exclude
    List<Course> courses = new ArrayList<>();
}
